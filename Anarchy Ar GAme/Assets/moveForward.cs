﻿using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class moveForward : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    // Start is called before the first frame update
   
   public void OnPointerDown(PointerEventData data) {

       PlayerController.mvfwd = true;
        PlayerController.idle = false;

   }

   public void OnPointerUp(PointerEventData data){

       PlayerController.mvfwd = false;
       PlayerController.idle = true;

   }
   
}
