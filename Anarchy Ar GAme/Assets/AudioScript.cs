﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioScript : MonoBehaviour
{

     public AudioClip [] playeraudioClip;
     public AudioSource playerSource;
     public static AudioScript instance;
      
  


    // Start is called before the first frame update
    void Start()
    { if(instance==null){
            instance=this;
        }
           playerSource = GetComponent<AudioSource>();
           PlayAudio(0);
        
        
    }

    void Update()
    {
    
       
    }
      public void PlayAudio(int clip){

         playerSource.clip = playeraudioClip[clip];
         playerSource.Play();
        

     }
}
