﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemycontroller : MonoBehaviour
{
    public Transform playerTransform;
    public int enemyHealth = 100;
    public static enemycontroller instance;
    private Vector3 direction;
    static Animator enemyanim;
    public  AnimationState animState;
    protected bool headhit=false;
    protected bool bodyhit=false;
    public Slider enemyHealthBar;
    public  float distancefwd;
   public  float distancebwd;
   public float kickdistance;
   public float rightpunchdistance;
   public float leftpunchdistance;
   public float combodistance;
   public BoxCollider [] enemycolliders;
   public Vector3 enemyposition;
   public GameObject [] enemyclone;
   
   void Awake(){
       if (instance == null){
           instance = this;
       }
   }
   
    void Start()
    {
        enemyanim = GetComponent<Animator>();
        SetAllEnemyColiders(false);
        enemyposition = transform.position;
    }
      private void SetAllEnemyColiders(bool state){
        for(int i = 0; i < enemycolliders.Length; i++)
        {
           enemycolliders[i].enabled = state;
        }
    }

    // Update is called once per frame
    void Update()
    { 
        if (enemyanim.GetCurrentAnimatorStateInfo(0).IsName("fight idle")){
        direction = playerTransform.position - this.transform.position;
         direction.y=0;
         this.transform.rotation = Quaternion.Slerp(this.transform.rotation,Quaternion.LookRotation(direction),0.3f);
         SetAllEnemyColiders(false);
        }
        if (GameController.allowMovement == true) {

        if(direction.magnitude > distancefwd && direction.magnitude > kickdistance){
        SetAllEnemyColiders(false);
        enemyanim.SetTrigger("walkfwd");
        
        }
       else{enemyanim.ResetTrigger("walkfwd");
        }

        if(direction.magnitude <= distancebwd){
         SetAllEnemyColiders(false);
        enemyanim.SetTrigger("walkbwd");
        
        }
         else{enemyanim.ResetTrigger("walkbwd");
        }

        if(direction.magnitude < kickdistance && direction.magnitude > rightpunchdistance){
        enemycolliders[2].enabled = true;
        enemyanim.SetTrigger("rightfootkick");
        //AudioScript.instance.PlayAudio(13);
        }
         else{enemyanim.ResetTrigger("rightfootkick");
        }

        if(direction.magnitude > combodistance && direction.magnitude > distancefwd){
        enemycolliders[3].enabled = true;
        enemyanim.SetTrigger("flyingkick");
        }
        else{enemyanim.ResetTrigger("flyingkick");
        }

        if(direction.magnitude < rightpunchdistance && direction.magnitude > leftpunchdistance){
        enemycolliders[1].enabled = true;
        enemyanim.SetTrigger("rightpunch");
        //AudioScript.instance.PlayAudio(12);
        }
        else{enemyanim.ResetTrigger("rightpunch");
           
        }
        if(direction.magnitude < leftpunchdistance){
        enemycolliders[0].enabled = true;
        enemyanim.SetTrigger("leftpunch");
       // AudioScript.instance.PlayAudio(12);
        }
        else{enemyanim.ResetTrigger("leftpunch");
           
        }
     }
}

    public void EnemyReactionBodyHit(){
         enemyanim.ResetTrigger("fight idle");
        enemyanim.SetTrigger("BodyHit");
        AudioScript.instance.PlayAudio(14);
        bodyhit = true;
        ChekEnemyLife();
       
    }
    public void EnemyReactionHeadHit(){
        enemyanim.ResetTrigger("fight idle");
        enemyanim.SetTrigger("HeadHit");
        AudioScript.instance.PlayAudio(14);
        headhit=true;
        ChekEnemyLife();
    }

    public void ChekEnemyLife(){
        if (enemyHealth>0 && bodyhit==true){
        enemyHealth = enemyHealth-2;
        enemyHealthBar.value = enemyHealth;
        bodyhit=false;
        }
         if (enemyHealth>0 && headhit == true){
        enemyHealth = enemyHealth-4;
        enemyHealthBar.value = enemyHealth;
        headhit=false;
        }
        if (enemyHealth<=0 &&  GameController.allowMovement == true){
        enemyanim.SetTrigger("KO");
        AudioScript.instance.PlayAudio(15);
        GameController.allowMovement = false;
        GameController.instance.scorePlayer();
        StartCoroutine (EnemyresetCharacters());

        if(GameController.playerscore==2){
         GameController.allowMovement=false;
         PlayerController.instance.StartCoroutine(PlayerController.instance.playerWins());
         }
        }
         
    }
   
    public void resetEnemyStats(){
        enemyHealth=100;
        enemyHealthBar.value=100;
        
 }

     IEnumerator EnemyresetCharacters(){
      yield return new WaitForSeconds(6);
     
      enemyHealth=100;
      enemyHealthBar.value=100;
      PlayerController.instance.resetPlayerStats();
       enemyanim.SetTrigger("fight idle");
       enemyanim.ResetTrigger("KO");
       GameController.instance.StartCoroutine(GameController.instance.beginPlay());

      
      
     
    }
  

        public IEnumerator EnemyWins(){
         yield return new WaitForSeconds(6);
        enemyanim.SetTrigger("arisawon");
       AudioScript.instance.PlayAudio(7);
       enemyanim.ResetTrigger("fight idle");
      
     
    }

    

   



}
