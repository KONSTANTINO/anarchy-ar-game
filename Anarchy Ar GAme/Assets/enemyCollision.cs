﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class enemyCollision : MonoBehaviour
{
  
     void OnTriggerEnter(Collider other){

      if (PlayerController.attacking == true){
       if(other.tag == "Player.Left.Foot" || other.tag == "Player.Right.Foot" && other.tag =="Enemy.Head"){
        enemycontroller.instance.EnemyReactionBodyHit();
       }
       if(other.tag == "Player.Right.Hand"|| other.tag == "Player.Left.Hand" && other.tag =="Enemy.Head"){
        enemycontroller.instance.EnemyReactionHeadHit();
        
       }
    }
     
  }     
  
}
