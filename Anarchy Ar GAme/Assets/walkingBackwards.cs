﻿using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class walkingBackwards : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
   
   public void OnPointerDown(PointerEventData data) {

       PlayerController.mvBack = true;
       PlayerController.idle = false;


   }

   public void OnPointerUp(PointerEventData data){

       PlayerController.mvBack = false;
       PlayerController.idle = true;

   }
   
}
