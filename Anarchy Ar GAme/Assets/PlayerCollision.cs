﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    

       void OnTriggerEnter(Collider other){

       if(other.tag == "Enemy.Left.Foot" || other.tag == "Enemy.Right.Foot" && other.tag =="Player.Head"){
        PlayerController.instance.PlayerReactionBodyHit();
       }
       if(other.tag == "Enemy.Right.Hand"|| other.tag == "Enemy.Left.Hand" && other.tag =="Player.Head"){
        PlayerController.instance.PlayerReactionHeadHit();
       }
    }   

  
}


