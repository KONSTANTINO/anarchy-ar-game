﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public static bool allowMovement=false;
    public GameObject [] ui;
    public GameObject [] controlls;
    private bool play321 = false;
    public static int playerscore=0;
     public static int enemyscore=0;
    


   
    // Start is called before the first frame update

    void Awake(){
        if(instance == null){
            instance = this;
        }
    }
    void Start()
    {
     AudioScript.instance.PlayAudio(0);
        
    }
   

    // Update is called once per frame
    void Update()
    {
              if (play321==false){
                if(DefaultTrackableEventHandler.trueFalse == false)
                {

                    for(int i = 0; i < controlls.Length; i++)
                    {
                      controlls[i].SetActive(false);
                    }
                
                }
             
                if(DefaultTrackableEventHandler.trueFalse == true)
                {

                    for(int i = 0; i < controlls.Length; i++)
                    {
                      controlls[i].SetActive(true);
                    }
                    for(int i = 0; i < ui.Length; i++)
                    {
                      ui[i].SetActive(false);

                    }
                    
                     play321=true;
                     StartCoroutine(beginPlay());
                }
            }   
           
            

        }
        
     public IEnumerator beginPlay(){
       AudioScript.instance.PlayAudio(2);
      yield return new WaitForSeconds(5);
      if(PlayerController.instance.rounds==1){ AudioScript.instance.PlayAudio(3);}
      if(PlayerController.instance.rounds==2){ AudioScript.instance.PlayAudio(4);}
      if(PlayerController.instance.rounds==3){ AudioScript.instance.PlayAudio(5);}
       allowMovement=true;
    }
   
     public void scorePlayer(){ playerscore = playerscore + 1;
    
     }
    public void scoreEnemy(){enemyscore= enemyscore+1;
  
    }

    
}
