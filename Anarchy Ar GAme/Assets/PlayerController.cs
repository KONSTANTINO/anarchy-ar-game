﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform enemyTarget;
    static Animator anim;
    public static bool mvBack = false;
    public static bool mvfwd = false;
    public static bool idle = true;
    public static bool attacking = false;
    private Vector3 direction;
    public int playerHealth = 100;
    public static PlayerController instance;
      public Slider playerHealthBar;
      protected bool bodyhit=false;
      protected bool headhit=false;
      public BoxCollider [] playercolliders;
     
      public Vector3 playerposition;
      public int rounds=1;
      

    void Awake()
    {
        if(instance==null){
            instance=this;
        }
    }


    void Start(){
      anim = GetComponent<Animator>();
      SetAllPlayerColiders(false);
      playerposition = transform.position;
     
     

      
      
    }
     
    


      private void SetAllPlayerColiders(bool state){
        for(int i = 0; i < playercolliders.Length; i++)
        {
           playercolliders[i].enabled = state;
        }
    }

    // Update is called once per frame
    void Update(){

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Fighting Idle")){
             direction = enemyTarget.position - this.transform.position;
             direction.y=0;
             this.transform.rotation = Quaternion.Slerp(this.transform.rotation,Quaternion.LookRotation(direction),0.3f);
              attacking=false;
              SetAllPlayerColiders(false);
        }


        if (attacking==false){
        if(mvBack == true) {
            anim.SetTrigger("wlkBack");
            anim.ResetTrigger("idle");
            SetAllPlayerColiders(false);
        }
        else if(idle == true){
            anim.SetTrigger("idle");
            anim.ResetTrigger("wlkBack");
            anim.ResetTrigger("wlkforwd");
            SetAllPlayerColiders(false);
        }

        if(mvfwd == true) {
          anim.SetTrigger("wlkforwd");
          anim.ResetTrigger("idle");
          SetAllPlayerColiders(false);
        }
        
    }
   
    else if(attacking==true){
        SetAllPlayerColiders(true);
   }                 

}       
 
        public void punch(){
            
             attacking=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("PUN");
            
            AudioScript.instance.PlayAudio(8);
            playercolliders[2].enabled = true;
            playercolliders[0].enabled = false;
            playercolliders[1].enabled = false;
            playercolliders[3].enabled = false;
        }
    

        public void JabToElbowPunch(){
            attacking=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("JTEP");
            AudioScript.instance.PlayAudio(8);
            playercolliders[3].enabled = true;
            playercolliders[2].enabled = false;
            playercolliders[0].enabled = false;
            playercolliders[1].enabled = false;
          
        }

        public void RoundHouseKick(){
            attacking=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("RHK1");
            AudioScript.instance.PlayAudio(9);
            playercolliders[0].enabled = true;
             playercolliders[3].enabled = false;
            playercolliders[2].enabled = false;
            playercolliders[1].enabled = false;
          
        }

        public void InsideCrescentKick(){
            attacking=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("ICK");
            AudioScript.instance.PlayAudio(9);
            playercolliders[1].enabled = true;
            playercolliders[0].enabled = false;
            playercolliders[3].enabled = false;
            playercolliders[2].enabled = false;
            
        }

        public void PlayerReactionBodyHit(){
            AudioScript.instance.PlayAudio(10);
            CheckPlayerLife();
            bodyhit=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("Bodyhit");
           
            
        }
        public void PlayerReactionHeadHit(){
            AudioScript.instance.PlayAudio(10);
            CheckPlayerLife();
            headhit=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("Headhit");
         
            
        }
        public void Combo1(){
             
             attacking=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("Combo1");
            AudioScript.instance.PlayAudio(8);
            AudioScript.instance.PlayAudio(9);
            playercolliders[1].enabled = true;
            playercolliders[2].enabled = true;
            playercolliders[0].enabled = false;
            playercolliders[3].enabled = false;
           
           
        }
        public void Combo2(){
            attacking=true;
            anim.ResetTrigger("idle");
            anim.SetTrigger("Combo2");
            AudioScript.instance.PlayAudio(9);
            AudioScript.instance.PlayAudio(8);
            playercolliders[2].enabled = true;
            playercolliders[3].enabled = true;
            playercolliders[1].enabled = false;
            playercolliders[0].enabled = false;
        }
      

   
         public void CheckPlayerLife(){
        if (playerHealth>0 && bodyhit == true){
        playerHealth= playerHealth-2;
        playerHealthBar.value = playerHealth;
        bodyhit=false;
        }
         if (playerHealth>0 && headhit == true){
        playerHealth = playerHealth-4;
        playerHealthBar.value = playerHealth;
        headhit=false;
        }
        if (playerHealth<=0 && GameController.allowMovement == true){
         anim.SetTrigger("KO");
         AudioScript.instance.PlayAudio(11);
         GameController.allowMovement = false;
         GameController.instance.scoreEnemy();
         StartCoroutine (PlayerresetCharacters());
         
         } 
        if(GameController.enemyscore==2){
        GameController.allowMovement=false;
        enemycontroller.instance.StartCoroutine(enemycontroller.instance.EnemyWins());
        }
      
         
         

      
    }
        public void resetPlayerStats(){
         playerHealth=100;
         playerHealthBar.value=100;
          rounds=rounds+1;

        }
  
      IEnumerator PlayerresetCharacters(){
      
      yield return new WaitForSeconds(6);
      anim.SetTrigger("idle");
      anim.ResetTrigger("KO");
      rounds=rounds+1;
      GameController.instance.StartCoroutine(GameController.instance.beginPlay());
      playerHealth=100;
      playerHealthBar.value=100;
     enemycontroller.instance.resetEnemyStats();
     
    } 

     public IEnumerator playerWins(){
    yield return new WaitForSeconds(6);
    anim.SetTrigger("kinghtwon");
    anim.ResetTrigger("idle");
    AudioScript.instance.PlayAudio(6);
    GameController.allowMovement = false;
    SetAllPlayerColiders(false);
     
     
    }


}   
